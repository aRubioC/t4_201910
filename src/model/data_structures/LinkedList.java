package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;
//Tomado de https://algs4.cs.princeton.edu/code/edu/princeton/cs/algs4/LinkedStack.java.html
//Modificado por: Arturo Rubio
public class LinkedList<T> implements ILinkedList<T>
{
	private Node<T> start;
	
	private Node<T> end;
	
	private int size;
	
	public LinkedList()
	{
		start = null;
		
		end = null;
		
		size = 0;
	}

	

	@Override
	public int getSize() 
	{		
		return size;
	}

	@Override
	public void add(T value) 
	{
		Node<T> ness = new Node<T>(value);
			ness.setNext(start);
			start = ness;
		size++;
	}

	@Override
	public void addAtEnd(T value) 
	{	
		Node<T> ness = new Node<T>(value);
		if(start == null)
		{
			start = ness;
		}
		else
		{
			if(end == null)
			{
				calculateSize();
			}			
			end.setNext(ness);
			end = ness;
		}
		size++;
	}

	@Override
	public void addAtk(int index, T value) 
	{
		// TODO Auto-generated method stub
		int i = 0;
		Node<T> prev = null;
		Node<T> sat = start;
		Node<T> ness = new Node<T>(value);
		if(start == null)
		{
			start = ness;
		}
		else
		{
			if(index == 0)
			{
				add(value);
			}
			else
			{
				while(i < index && sat.getNext() != null)
				{		
					prev = sat;	
					i++;
					sat = sat.getNext();
				}
				
				ness.setNext(prev.getNext());
				prev.getNext().setPrevious(ness);
				ness.setPrevious(prev);
				prev.setNext(ness);
			}
	
		}
		
		size++;
	}

	@Override
	public Node<T> getElement(int index) 
	{
		boolean finded = false;
		Node<T> ans = null;
		Node<T> sat = start;
		for(int i = 0; i < size && finded == false; i++)
		{
			if(i == index)
			{
				ans = sat;
				finded = true;
			}
			sat = sat.getNext();
		}
		return ans;
	}

	@Override
	public Node<T> getCurrentElement() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete() 
	{
		// TODO Auto-generated method stub
		if(start == null)
		{
			return;
		}
		else
		{
			start = start.getNext();
			start.setPrevious(null);
		}
		size--;
	}

	@Override
	public void deleteAtk(int index) 
	{
		// TODO Auto-generated method stub
		Node<T> ness = getElement(index);
		if(index == 0)
		{
			Node<T> starte = start;
			start = start.getNext();
			start.setPrevious(null);
			starte.setNext(null);
			
		}
		else
		{
			ness.getPrevious().setNext(ness.getNext());
			ness.getNext().setPrevious(ness.getPrevious());
			ness.setNext(null);
			ness.setPrevious(null);
		}
		size--;
	}

	@Override
	public Node<T> next() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Node<T> previous() 
	{
		// TODO Auto-generated method stub
		return null;
	}

	public void calculateSize()
	{
		size = 0;
		Node<T> current = start;
		while(current != null)
		{
			size ++;
			end = current;
			current = current.getNext();
		}
	}



	@Override
	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	
	 private class ListIterator implements Iterator<T> 
	 {
	        private Node<T> current = start;
	        public boolean hasNext()  
	        { 
	        	return current != null;                     
	        }
	        public void remove()      
	        { 
	        	throw new UnsupportedOperationException();  
	        }

	        public T next() 
	        {
	            T item = current.getValue();
	            current = current.getNext(); 
	            return item;
	        }
	    }

	
}
