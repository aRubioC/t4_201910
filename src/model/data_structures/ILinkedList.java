package model.data_structures;

/**
 * Abstract Data Type for a linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface ILinkedList<T> extends Iterable<T>
{

	
	
	int getSize();
	
	void add(T value);
	
	void addAtEnd(T value);
	
	void addAtk(int index, T value);
	
	Node<T> getElement(int index);
	
	Node<T> getCurrentElement();
	
	void delete();
	
	void deleteAtk(int index);
	
	Node<T> next();
	
	Node<T> previous();

}
