package model.data_structures;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MovingViolations implements Comparable<MovingViolations>
{

	private int objectId;
	
	private String location;
	
	private String ticketIssueDate;
	
	private int totalPaid;
	
	private String accidentIndicator;
	
	private String violationDescription;
	
	private Date ticketDate;
	
	public MovingViolations(int id, String loc, String date, int paid, String acc, String viol) 
	{
		objectId = id;
		
		location = loc;
		
		ticketIssueDate = date;
		
		totalPaid = paid;
		
		accidentIndicator = acc;
		
		violationDescription = viol;
		
		try 
		{
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
			ticketDate = df.parse(date);
		}
		catch(Exception e)
		{
			ticketDate = null;
		}
	}
	
	public int getObjectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	
	
	
	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		// TODO Auto-generated method stub
		return ticketIssueDate;
	}
	
	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}
	
	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}
		
	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}
	
	public Date getTicketDate()
	{
		return ticketDate;
	}

	@Override
	public int compareTo(MovingViolations o) 
	{
		// TODO Auto-generated method stub
		int resultado = 0;
		
		if(ticketDate != null)
		{
			if(this.getTicketDate().before(o.getTicketDate()))
			{
				resultado = 1;
			}
			else if(this.getTicketDate().after(o.getTicketDate()))
			{
				resultado = -1;
			}
			else
			{
				if(this.getObjectId() > o.getObjectId())
				{
					resultado = 1;
				}
				else if(this.getObjectId() < o.getObjectId())
				{
					resultado = -1;
				}
				else 
				{
					resultado = 0;
				}
			}
		}
		else
		{
			if(this.getObjectId() > o.getObjectId())
			{
				resultado = 1;
			}
			else if(this.getObjectId() < o.getObjectId())
			{
				resultado = -1;
			}
			else 
			{
				resultado = 0;
			}
		}
		
		return resultado;
	}
	
	public String toString()
	{
	
		return objectId + " / " + location + " / " + ticketIssueDate + " / " + totalPaid + " / " + violationDescription;
	}
}
