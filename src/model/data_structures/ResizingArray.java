package model.data_structures;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ResizingArray<T> implements IResizingArray<T> 
{

	private T[] t;
	
	private int size;
	
	public ResizingArray() 
	{
		// TODO Auto-generated constructor stub
		  t = (T[]) new Object[1000];
		  size = 0;
	}
	
	@Override
	public boolean isEmpty() 
	{
		// TODO Auto-generated method stub
		return size==0;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void resize(int capacity) 
	{
		// TODO Auto-generated method stub
		assert capacity >= size;
        T[] temp = (T[]) new Object[capacity];
        for (int i = 0; i < size; i++)
            temp[i] = t[i];
        t = temp;
	}

	@Override
	public void add(T item) 
	{
		// TODO Auto-generated method stub
		if(size == t.length)
			resize(2*t.length);
		t[size++] = item;
	}
	
	public T get(int index)
	{
		return t[index];
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new ArrayIterator();
	}
	
	private class ArrayIterator implements Iterator<T> 
	{
        private int i = 0;
        
        public boolean hasNext()  
        { 
        	return i < size;                               
        }
        
		@Override
		public T next() 
		{
			// TODO Auto-generated method stub
			if (!hasNext()) 
            	throw new NoSuchElementException();
            return t[i++];
		}
     }


}
