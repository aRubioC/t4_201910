package model.data_structures;

import java.util.Iterator;

public interface IResizingArray <T> extends Iterable<T>
{
	public boolean isEmpty();
	
	public int size();
	
	void resize(int capacity);
	
	public void add(T item);
	
	public Iterator<T> iterator();

}
