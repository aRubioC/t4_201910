package model.util;

import java.util.Comparator;
import java.util.concurrent.Exchanger;

import model.data_structures.SdtRandom;

//Algoritmos tomados de https://algs4.cs.princeton.edu/code/

public class Sort {
	
	private static final int CUTOFF = 7;
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) 
	{		
		// TODO implementar el algoritmo ShellSort
		int n = datos.length;

        // 3x+1 increment sequence:  1, 4, 13, 40, 121, 364, 1093, ... 
        int h = 1;
        while (h < n/3) h = 3*h + 1; 

        while (h >= 1) {
            // h-sort the array
            for (int i = h; i < n; i++) {
                for (int j = i; j >= h && less(datos[j], datos[j-h]); j -= h) 
                {
                    exchange(datos, j, j-h);
                }
            }

            h /= 3;
        }
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) 
	{
		Comparable[] aux = datos.clone();
        sortM(aux, datos, 0, datos.length-1);  
        assert isSorted(datos);
	}
	
	 private static void merge(Comparable[] src, Comparable[] dst, int lo, int mid, int hi) {

	        // precondition: src[lo .. mid] and src[mid+1 .. hi] are sorted subarrays
	        assert isSorted(src, lo, mid);
	        assert isSorted(src, mid+1, hi);

	        int i = lo, j = mid+1;
	        for (int k = lo; k <= hi; k++) {
	            if      (i > mid)              dst[k] = src[j++];
	            else if (j > hi)               dst[k] = src[i++];
	            else if (less(src[j], src[i])) dst[k] = src[j++];   // to ensure stability
	            else                           dst[k] = src[i++];
	        }

	        // postcondition: dst[lo .. hi] is sorted subarray
	        assert isSorted(dst, lo, hi);
	    }

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) 
	{
		// TODO implementar el algoritmo QuickSort
		SdtRandom.shuffle(datos);
        sortQ(datos, 0, datos.length - 1);
        assert isSorted(datos);	
	}
	
	private static void sortQ(Comparable[] a, int lo, int hi) { 
	        if (hi <= lo) return;
	        int lt = lo, gt = hi;
	        Comparable v = a[lo];
	        int i = lo + 1;
	        while (i <= gt) {
	            int cmp = a[i].compareTo(v);
	            if      (cmp < 0) exchange(a, lt++, i++);
	            else if (cmp > 0) exchange(a, i, gt--);
	            else              i++;
	        }

	        // a[lo..lt-1] < v = a[lt..gt] < a[gt+1..hi]. 
	        sortQ(a, lo, lt-1);
	        sortQ(a, gt+1, hi);
	        assert isSorted(a, lo, hi);
	    }
	
	 private static void sortM(Comparable[] src, Comparable[] dst, int lo, int hi) {
	        // if (hi <= lo) return;
	        if (hi <= lo + CUTOFF) { 
	            insertionSort(dst, lo, hi);
	            return;
	        }
	        int mid = lo + (hi - lo) / 2;
	        sortM(dst, src, lo, mid);
	        sortM(dst, src, mid+1, hi);

	        // if (!less(src[mid+1], src[mid])) {
	        //    for (int i = lo; i <= hi; i++) dst[i] = src[i];
	        //    return;
	        // }

	        if (!less(src[mid+1], src[mid])) {
	            System.arraycopy(src, lo, dst, lo, hi - lo + 1);
	            return;
	        }

	        merge(src, dst, lo, mid, hi);
	    }
	 private static void insertionSort(Comparable[] a, int lo, int hi) 
	 {
	        for (int i = lo; i <= hi; i++)
	            for (int j = i; j > lo && less(a[j], a[j-1]); j--)
	                exchange(a, j, j-1);
	 }
	
	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// TODO implementar
		return v.compareTo(w) < 0;
	}
	
	private static boolean less(Object a, Object b, Comparator comparator) 
	{
	        return comparator.compare(a, b) < 0;
	}
	
	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		// TODO implementar
		Comparable swap = datos[i];
        datos[i] = datos[j];
        datos[j] = swap;
	}
	
	 private static boolean isSorted(Comparable[] a) {
	        return isSorted(a, 0, a.length - 1);
	    }

	    private static boolean isSorted(Comparable[] a, int lo, int hi) {
	        for (int i = lo + 1; i <= hi; i++)
	            if (less(a[i], a[i-1])) return false;
	        return true;
	    }

	    private static boolean isSorted(Object[] a, Comparator comparator) {
	        return isSorted(a, 0, a.length - 1, comparator);
	    }

	    private static boolean isSorted(Object[] a, int lo, int hi, Comparator comparator) {
	        for (int i = lo + 1; i <= hi; i++)
	            if (less(a[i], a[i-1], comparator)) return false;
	        return true;
	    }
	
}

