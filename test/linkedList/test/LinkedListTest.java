package linkedList.test;

import static org.junit.Assert.assertNotEquals;

import java.util.ArrayList;

import junit.framework.TestCase;
import model.data_structures.LinkedList;
import model.data_structures.Node;

//Tomado de cupi2. Ejemplo de nivel 9
//Modificado por Arturo Rubio


public class LinkedListTest<T> extends TestCase
{
	private LinkedList<T> linked;
	
	private int numeroNodos;
	
	private void setupEscenario1()
	{
		linked = new LinkedList<T>();
		numeroNodos = 0;
	}
	
	private void setupEscenario2()
	{
		linked = new LinkedList<T>();
		numeroNodos = 10;
		for(Integer i = 0; i < numeroNodos; i++)
		{
			linked.add((T) i);
		}
	}
	
	private void setupEscenario3()
	{
		linked = new LinkedList<T>();
		numeroNodos = 10;
		for(Integer i = 0; i < numeroNodos; i++)
		{
			linked.addAtEnd((T)i);
		}
	}
	
	public void testAgregar()
	{
		setupEscenario1( );
        numeroNodos = 11;
        ArrayList<Node<T>> linked2 = new ArrayList( );
        Node<T> lol = null;
        for( Integer cont = 0; cont < numeroNodos; cont++ )
        {    
            linked2.add(new Node<T>((T)cont) );
        }
        // Agrega los pacientes y verifica que se hayan agregado correctamente
        for(Integer cont = 0; cont < numeroNodos; cont++ )
        {
            Node<T> nodes = (Node<T>) linked2.get(cont);
            linked.add(nodes.getValue());
            ArrayList lesNodes = new ArrayList();
            for(int i = 0; i < linked.getSize(); i++)
            {
            	lesNodes.add(linked.getElement(i));
            }
            lol = linked.getElement(0);

            // Verifica que la informaci�n sea correcta
            assertEquals( "La adici�n no se realiz� correctamente", nodes.getValue(), lol.getValue() );

            // Verifica que el n�mero de pacientes en la lista sea el correcto
            assertEquals( "El n�mero de nodos no es el correcto", (cont + 1), lesNodes.size());
        }
	}
	
	public void testAgregarAtk()
	{
		setupEscenario2();
		numeroNodos = 11;
		ArrayList nodes = new ArrayList();
		Node<T> node;
		Node<T> aux;
		
		for(Integer i = 0; i < numeroNodos; i++)
		{
			node = new Node<T>((T)i);
			nodes.add(node);
		}
		
		node = (Node<T>) nodes.get(0);
		linked.add(node.getValue());
		
		int ced = 5;
		
		for(Integer cont = 1; cont < numeroNodos -1; cont ++)
		{
			node = (Node<T>) nodes.get(cont);
			linked.addAtk(ced, node.getValue());
			aux = linked.getElement(5);
			
			assertEquals("La adicion no se realizo correctamente", node.getValue(), aux.getValue());
		}
				
		
		
	}
	
	public void testEliminar()
	{
		setupEscenario3();
		linked.delete();
		Node<T> nodes = linked.getElement(0);
		
		assertNotSame("El nodo no debio encontrarse", 0, nodes.getValue());
		assertEquals("El numero de nodos no es el correcto", (numeroNodos - 1), linked.getSize());
	}
	
	public void testEliminarAtk()
	{
		
	}
}
